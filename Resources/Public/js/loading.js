window.Aloha.ready( function() {
	
	var $ = window.Aloha.jQuery;
	var viewFrameId = 'tx_pxafoundation_viewpage_iframe';
	
	// For back-compatibility
	window.alohaQuery = $;
	
	var isActive = false;
	
	function recallState() {
	
		var cookieData = document.cookie;
		if (cookieData) {
			
			var pairs = cookieData.split(/;\s+?/);
			var params = {};
			pairs.forEach(function(pair){
				var items = pair.split("=");
				params[items[0]] = items[1];
			});

			if (params['Typo3Aloha_isActive']) {
				if (params['Typo3Aloha_isActive'] == '1') {
					isActive = true;
					$('#aloha-onoff').text($('#aloha-onoff').data('text-deactivate'));
				}
				else {
					isActive = false;
					$('#aloha-onoff').text($('#aloha-onoff').data('text-activate'));
				}
			}
		}
	
	}

	recallState();
	
		// If not in iframe, show "loader"
	if(self==top) {
		// $('#aloha-not-loaded').show();
		//$('body').css('margin-top','47px');
	}

	$('.alohaeditable').aloha();

		// If not in iframe, show topbar and hide loader, hide in iframe
	setTimeout(function () {
		if(self==top) {
			$('#aloha-not-loaded').hide();
			$('#aloha-top-bar').fadeIn("slow");
		} else {
			$('#aloha-not-loaded').hide();
			$('#aloha-top-bar').hide();
				// Update save and discard
			$.ajax({
				url: alohaUrl,
				data: ({
					'action':'updateSaveState'
				}),
				cache: false,
				dataType : 'html',
				type: 'POST',
				success: function(xhr){
					$().el7r_notify({
						'text':xhr,
						'skin':'hidden'
					});
				},
				error: function(xhr){
				}
			});
		}
		
		updateState();
		
	});

	$('#aloha-discardButton').click(function() {
			// Check if iframe exists, then commit save from that instead to get the correct page id
		if ($('#tx_pxafoundation_viewpage_iframe').length) {
			var iframe = $('#tx_pxafoundation_viewpage_iframe');
			$('#aloha-discardButton', iframe.contents()).trigger('click');
		} else {
			$.ajax({
				url: alohaUrl,
				data: ({
					'action':'discardSavings'
				}),
				cache: false,
				dataType : 'html',
				type: 'POST',
				success: function(xhr){
					$().el7r_notify({
						'text':xhr,
						'skin':'success'
					});
					$('#aloha-saveButton').hide();
				},
				error: function(xhr){
					$().el7r_notify({
						'text':xhr.responseText,
						'skin':'darkred'
					});
				}
			});
		}
	});
	$('#aloha-saveButton').click(function() {
			// Check if iframe exists, then commit save from that instead to get the correct page id
		if ($('#tx_pxafoundation_viewpage_iframe').length) {
			var iframe = $('#tx_pxafoundation_viewpage_iframe');
			$('#aloha-saveButton', iframe.contents()).trigger('click');
		} else {
			$.ajax({
				url: alohaUrl,
				data: ({
					'action':'commitSavings'
				}),
				cache: false,
				dataType : 'html',
				type: 'POST',
				success: function(xhr){
					$().el7r_notify({
						'text':xhr,
						'skin':'success'
					});
					$('#aloha-saveButton').hide();
				},
				error: function(xhr){
					$().el7r_notify({
						'text':xhr.responseText,
						'skin':'darkred'
					});
				}
			});
		}
	});

	$('#alohaeditor-icon').click(function(el) {
		$('#alohaeditor-icon').toggleClass('active');
		$('#alohaeditor-info').toggle();
	});
	
	/**
	 * turn aloha on or off
	 */
	
	function updateState() {
		if (isActive) {
			activate();
		}
		else {
			deactivate();
		}
		
	}
	
	function activate() {
		var iframe = $('iframe#' + viewFrameId)[0];
		
		$('#aloha-top-bar').removeClass('collapsed');
		$('.alohaeditable')
			.attr('contenteditable', true)
			.addClass('aloha-editable')
			.addClass('alohaeditable-block')
			.addClass('action-edit')
		;
		if(iframe) {
			iframe.contentWindow.jQuery('.alohaeditable')
				.attr('contenteditable', true)
				.addClass('aloha-editable')
				.addClass('alohaeditable-block')
				.addClass('action-edit')
		}
		
		$('#aloha-onoff').text($('#aloha-onoff').data('text-deactivate'));
		document.cookie = "Typo3Aloha_isActive=1";
		$('body').removeClass('aloha_inactive').addClass('aloha_active');
	}
	
	function deactivate() {
		var iframe = $('iframe#' + viewFrameId)[0];
		$('#aloha-top-bar').addClass('collapsed');
		$('.alohaeditable')
			.attr('contenteditable', false)
			.removeClass('aloha-editable')
			.removeClass('aloha-editable-active')
			.removeClass('aloha-editable-highlight')
			.removeClass('alohaeditable-block')
			.removeClass('action-edit')
		;
		
		if(iframe) {
			iframe.contentWindow.jQuery('.alohaeditable')
				.attr('contenteditable', false)
				.removeClass('aloha-editable')
				.removeClass('aloha-editable-active')
				.removeClass('aloha-editable-highlight')
				.removeClass('alohaeditable-block')
				.removeClass('action-edit');
		iframe.contentWindow.jQuery('.aloha-ui-toolbar')
				.remove();
				
		}
		
		$('#aloha-onoff').text($('#aloha-onoff').data('text-activate'));
		document.cookie = "Typo3Aloha_isActive=0";
		$('body').removeClass('aloha_active').addClass('aloha_inactive');
	}
	
	$('#aloha-onoff').click(function(e) {
		isActive = !isActive;
		updateState();
	});
});