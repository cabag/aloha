# Aloha TYPO3 Frontend Editing

Allowing to have inline frontend editing in TYPO3:

* Seamless integration into frontend
* Realtime storage of changes
* Responsive device testing (desktop, tablet, smartphone)
* Edit titles and text inline
* Open edit form in lightbox (edit everything in frontend)
* Add pages
* Add elements
* Easy extendable
	* Simple integration in TypoScript
	* ExtBase FLUID ViewHelper for your extensions
	* Add editing capabilities to any of your extensions


## Installation:

1. get sourcecode:
	cd typo3conf/ext/
	git clone git@bitbucket.org:cabag/aloha.git
2. Go to extension manager and install it
3. Load Aloha TypoScript in your TypoScript template
4. Activate it in your user settings module
