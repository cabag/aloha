[globalVar = TSFE : beUserLogin > 0]

	# Additional page plugin for save requests as those need a BE-USER
	aloha = PAGE
	aloha{
		typeNum = 661

		config {
			disableAllHeaderCode = 1
			xhtml_cleaning = none
			admPanel = 0
			metaCharset = utf-8
			no_cache = 1
		}

		includeLibs.aloha = EXT:aloha/Classes/Aloha/Save.php
		10 = USER
		10 {
			userFunc = Tx_Aloha_Aloha_Save->start
		}
	}
[global]


# Check if aloha is enabled, need to make some changes in ts
[globalVar = TSFE : beUserLogin > 0] && [userFunc = isAlohaEnabledForUser]
		
	# Disable realurl we don't break links in bodytext
	# TODO: see if we can disable realurl only for rtehtmlarea when aloha edit is enabled
	#config.tx_realurl_enable = 0
		
	# Make sure no_cache is set
	config.no_cache = 1
	
	# Disable spamProtectEmailAddresses so we don't break email links
	config.spamProtectEmailAddresses = 0

	# Tell browser to not cache if alohaEdit is set
	page.headerData.2014 = TEXT
	page.headerData.2014.value (\r\n
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />
	)
	

[global]

# Add t3link attribute to links
[globalVar = TSFE : beUserLogin > 0]

	# ads original link parameters to atag so we don't have to disable realurl
	lib.parseFunc_RTE.tags.link.typolink.ATagParams = t3link="{parameters:allParams}"
	lib.parseFunc_RTE.tags.link.typolink.ATagParams.insertData = 1

[global]

